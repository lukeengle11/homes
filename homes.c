#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "homes.h"


// Write a function called readhomes that takes the filename
// of the homes file and returns an array of pointers to home structs
// terminated with a NULL
struct home ** readhomes(char *filename)
{
    FILE *f = fopen(filename, "r");
    
    int size = 100; // size of homes array
    int count = 0; // Index number
    
    // Temporary variables
    int zip;
    char addr[40];
    int price;
    int area;
    
    struct home **homes = (struct home **)malloc(size * sizeof(struct home*));
    
    // Scans the file and puts information into the temporary variables
    while (fscanf(f, "%d,%[^,],%d,%d", &zip, addr, &price, &area) != EOF)
    {
        struct home *h = (struct home *)malloc(sizeof(struct home));
        h->zip = zip;
        h->addr = (char *)malloc((strlen(addr) + 1) * sizeof(char *));
        strcpy(h->addr, addr);
        h->price = price;
        h->area = area;
        if(count == size) // Increases the size of the array of it gets too big
        {
            size = size + 100;
            struct home **newarr = (struct home **)realloc(homes, size * sizeof(struct home *));
            if (newarr != NULL) homes = newarr;
            else exit(1);
        }
        homes[count] = h;
        
        count++;
    }
    if(count == size) // If the array is full, increase its size by one for a null character
    {
        size = size + 1;
        struct home **newarr = (struct home **)realloc(homes, size * sizeof(struct home *));
        if (newarr != NULL) homes = newarr;
        else exit(1);
    }
    homes[count] = NULL;
    
    fclose(f);
    return homes;
}

// Returns how many houses are in the listings
int homesCount(struct home **homes)
{
    int count = 0;
    while (homes[count] != NULL)
    {
        count++;
    }
    
    return count;
}

// Prompts the user for a zip code. Makes sure they input a 5 digit number.
int promptZip()
{
    int done = 0;
    int zip = 0;
    char str[10];
    while (!done)
    {
        printf("Type in a zip code: ");
        scanf("%d", &zip);
        sprintf(str, "%d", zip);
        if (strlen(str) != 5)
        {
            printf("You need to enter a valid zip code.\n");
        }
        else done = 1;
    }
    return zip;
}

// Finds how many houses are in the given zip code
int zipHomes(int userZip, struct home **homes)
{
    int count = 0;
    for (int i = 0; i < homesCount(homes); i++)
    {
        if (homes[i]->zip == userZip) count++;
    }
    return count;
}

// Prompts the user for a price
int promptPrice()
{
    int price = 0;
    printf("Type in a price: ");
    scanf("%d", &price);
    return price;
}

// Finds the houses with the requested price and prints them out
void findPrices(int userPrice, struct home **homes)
{
    for (int i = 0; i < homesCount(homes); i++)
    {
        if (homes[i]->price == userPrice)
        {
            printf("%s %d $%d\n", homes[i]->addr, homes[i]->zip, homes[i]->price);
        }
    }
}

int main(int argc, char *argv[])
{
    // Use readhomes to get the array of structs
    struct home **homes = readhomes(argv[1]);

    // At this point, you should have the data structure built
    // How many homes are there? Write a function, call it,
    // and print the answer.
    printf("There are %d homes\n\n", homesCount(homes));
    
    // Prompt the user to type in a zip code.
    int zip = promptZip();
    
    // At this point, the user's zip code should be in a variable
    // How many homes are in that zip code? Write a function,
    // call it, and print the answer.
    printf("There are %d homes in that zip code\n", zipHomes(zip, homes));

    // Prompt the user to type in a price.
    int price = promptPrice();

    // At this point, the user's price should be in a variable
    // Write a function to print the addresses and prices of 
    // homes that are within $10000 of that price.
    findPrices(price, homes);
}
